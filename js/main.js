// Corect answers
var exercises = [
  "for (var i = 1; i <= 3 ; i++) {\n"+
  " console.log(i);\n" +
"}"
,
"for (var i = 1; i <= 10; i++) {\n" +
  "if (i % 2 !==0) {\n" +
    "console.log(i);\n" +
" }\n" +
"}"
,
"for (var y = 1 ; y <= 5; y++) {\n" +
  "for (var i = 1; i <= 3; i++) {\n" +
    "console.log(i);\n" +
  "}\n" +
"}\n"

];

var statements = [
  " 1.Write code that will print all numbers from 1 up to 3. ",
  " 2.Print all odd numbers from 1 up to 10. ",
  " 3.Print all numbers from 1 up to 5. Repeat that 3 times. "
];

var hints = ["Come ooon, This is easy!", "Use % to check for parity."]


// function that modifies console.log
function replaceConsoleLog(a) {
  console.log = function (val) {
    a.push(val);
  }
  a.length = 0;
}




// is equal new function
//
// var t = [1, 2, 3];
// var r = [1, 2, 3];
//
// function isEqual(a, b) {
//   if (a.length !== b.length) {
//     return false;
//   }
//   for (var i = 0 ; i < a.length; i++) {
// console.log(i)
//         if (a[i] !== b[i]) {
//             return false;
//         }
//
//   }
//   return true;
// }



function compare(a, b) {
  if (a.length === b.length) {
      for (var i = 0; i < a.length; i++) {
        if (a[i] === b[i]) {
          // document.getElementById("validation").innerHTML = "corect";
          document.getElementById("correct").style.color = "green";
        }else {
          document.getElementById("wrong").style.color = "red";
          // document.getElementById("validation").innerHTML = "gresiiiit";
        }
      }
  }else {
     document.getElementById("wrong").style.color = "red";
    // document.getElementById("validation").innerHTML = "gresiiiit";
  }
}



//Array to be filled with the users result from running the code with console.log()
var output = [];

// //Array to be filled with the corect result of the compiled code  from  var exercises[];
var corectResult = [];

function captureOutput() {

  // Catch users input and save it in var result
  var result = document.getElementById("exerciseAnswer").value

    replaceConsoleLog(output);

    // Execute user input code
    eval(result);


    replaceConsoleLog(corectResult);

    // Execute the correct code
    eval(exercises[ex]);

    //variable to store result in order to show the answer ???:)))
    var printAnswer =[];

    for (var i = 0 ; i < output.length; i++) {
      printAnswer = printAnswer + output[i] + '\n';
    }
    document.getElementById('seeCompiledCode').innerHTML = printAnswer;

    // Check if users answer is correct
    compare(corectResult, output);
}



function showExercise() {
  // color the wronf sign back to white
  document.getElementById("wrong").style.color = "white";

  // color the correct sign back to white
  document.getElementById("correct").style.color = "white";

  //If code is allready written,pressing the See Answer button  will clear the text area and show the correct answer
  document.getElementById('exerciseAnswer').value = " ";

  //If a result from a preavios exercise is shown, pressing the See Answer button  will clear the text area
  document.getElementById('seeCompiledCode').innerHTML = " ";
  document.getElementById('seeCompiledCode').innerHTML = "Result ";


for (var i = 0; i < exercises.length; i++) {
   var showCorrectAnswer = exercises[ex];
}
  document.getElementById('exerciseAnswer').value = showCorrectAnswer
  // document.getElementById('seeCompiledCode').value = printAnswer;

}

// Hints


 function showHints() {
   // clear the hints area
   document.getElementById('hintsTitle').innerHTML = "";
   for (var i = 0; i < hints.length; i++) {
     var showHints = hints[ex];
   }
   document.getElementById('showHints').innerHTML = showHints;
 }


//Jump to next exercise

// var ex stores the exercise number from var exercises[]
var ex = 0;

function nextExercise() {
  if (ex < exercises.length-1) {
    ex++;
  }
  //Go to next statement
  document.getElementById("statements").innerHTML = statements[ex];

  // color the wronf sign back to white
  document.getElementById("wrong").style.color = "white";

  // color the correct sign back to white
  document.getElementById("correct").style.color = "white";

  //empty the exerciseAnswer text area area where user writes code
  document.getElementById('exerciseAnswer').value = " ";

  // Fill placeholder of the JavaScript text area
  document.getElementById('exerciseAnswer').value = "  JavaScript ";

  // empty the text area where the result of the executed code is shown
  document.getElementById('seeCompiledCode').innerHTML = " ";

  // Fill placeholder of the Result text area
  document.getElementById('seeCompiledCode').innerHTML = "  Result";

  // empty the hints area when pressing next button
  document.getElementById('showHints').innerHTML = " ";

  // Empty the validation response when pressing next button
  // document.getElementById("validation").innerHTML = "";

}
// Start with the first statement
document.getElementById("statements").innerHTML = statements[ex];



function previousExercise() {
  if (ex < exercises.length) {
    ex--;
  }
  //Go to preavious statement
  document.getElementById("statements").innerHTML = statements[ex];

  // color the wrong sign back to white
   document.getElementById("wrong").style.color = "white";

   // color the correct sign back to white
    document.getElementById("correct").style.color = "white";

  //empty the exerciseAnswer text area area where user writes code
  document.getElementById('exerciseAnswer').value = " ";

  // Fill placeholder of the JavaScript text area
  document.getElementById('exerciseAnswer').value = "  JavaScript ";

  // empty the text area where the result of the executed code is shown
  document.getElementById('seeCompiledCode').innerHTML = " ";

  // Fill placeholder of the Result text area
  document.getElementById('seeCompiledCode').innerHTML = "  Result";

  // empty the hints area when pressing back button
  document.getElementById('showHints').innerHTML = " ";
}
